/****************************************************************************************
 * cmd.c
 *
 *  Created on: May 19, 2018
 *      Author: Mihail CHERCIU (mcherciu@gmail.com)
 ****************************************************************************************/


/****************************************************************************************
 * Included Files
 ****************************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <kernel/spinlock.h>
#include <platform.h>
#include <app.h>
#include "log/log.h"
#include "cmd/cmd.h"

/****************************************************************************************
 * Pre-processor Definitions
 ****************************************************************************************/


/****************************************************************************************
 * Private Data
 ****************************************************************************************/


/****************************************************************************************
 * Public Functions
 ****************************************************************************************/

int send_cmd(int action, cmd_t *cmd, event_t *event)
{
    if (!event) return -1;
    if (!cmd)   return -1;

    cmd->what = action;
    event_signal(event, false);

    return 0;
}
