#ifndef LEDS_H_
#define LEDS_H_

#include "cmd/cmd.h"
#include <kernel/timer.h>

/* LED colors */
enum {
    LED_GREEN = 0
};

/* GSM commands */
typedef enum {
    LEDS_CMD_NONE = 0,
    LEDS_CMD_GREEN_OFF,
    LEDS_CMD_GREEN_ON,
    LEDS_CMD_GREEN_BLINK,
    LEDS_CMD_GREEN_BLINK_SLOW
} leds_actions_t;

typedef struct {
    timer_t timer_on;
    timer_t timer_off;
    int     ms_blink;
    int     ms_freq;
    int     color;
} led_timer_t;

typedef void (*gpio_state_t)(bool);
typedef void (*reset_timing_t)(void);
typedef void (*set_timing_t)(int, int, int);

typedef struct {
    event_t        event;
    cmd_t          cmd;
    led_timer_t    timing;
    gpio_state_t   g;
    set_timing_t   set;
    reset_timing_t reset;
} led_device_t;

int leds_cmd(leds_actions_t action);

#endif  // LEDS_H_
