LOCAL_DIR := izirunf7_lk
LKMAKEROOT := ..
LKROOT := lk
DEVICES_LK ?= devices_lk
LKINC := $(LOCAL_DIR) $(DEVICES_LK)
DEFAULT_PROJECT ?= izirunf7_izigoboard
BUILDROOT ?= $(LOCAL_DIR)
TOOLCHAIN_PREFIX = arm-none-eabi-
